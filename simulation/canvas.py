# **************************************************************************** #
#                                                                              #
#                                                         :::      ::::::::    #
#    canvas.py                                          :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: tony <tony@student.42.fr>                  +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2020/03/26 21:48:43 by tony              #+#    #+#              #
#    Updated: 2020/03/29 06:09:53 by tony             ###   ########.fr        #
#                                                                              #
# **************************************************************************** #

from p5 import *
import random
from simulation.cell import Cell
from simulation.constants import *

class Canvas(object):
    def __init__(self, pos, height, width):
        self.pos = Vector(*pos)
        
        self.height = height - self.pos.y * 2
        self.width = width - self.pos.x * 2
        
        self.cells = []

        self.infected = []
        self.recovered = []

    def draw_canvas(self):
        stroke(255)
        no_fill()
        rect(tuple(self.pos), self.width, self.height)
        
    def draw_cells(self):
        for cell in self.cells:
            cell.draw()

    def generate_cells(self, n):
        self.cells = []

        for _ in range(n):
            x = random.randint(self.pos.x + CELL_SIZE, self.width - CELL_RADIUS)
            y = random.randint(self.pos.y + CELL_SIZE, self.height - CELL_RADIUS)
            cell = Cell(x, y)

            self.cells.append(Cell(x, y))

    def move_cells(self):
        for cell in self.cells:
            cell.move(self)

    def spread(self):
        for i, cell in enumerate(self.cells):
            for j, subcell in enumerate(self.cells):
                if i != j:
                    if cell.infected:
                        cell.infect(subcell)

    def out_of_bound(self, pos):
        xlimit = self.pos.x + self.width
        ylimit = self.pos.y + self.height

        is_lowx = pos.x - CELL_SIZE < self.pos.x
        is_lowy = pos.y - CELL_SIZE < self.pos.y
        is_highx = pos.x + CELL_SIZE > xlimit
        is_highy = pos.y + CELL_SIZE > ylimit
        return is_lowx or is_lowy or is_highx or is_highy

    def has_finished(self):
        return all(cell.infected for cell in self.cells)

    def get_profiles(self):
        for cell in self.cells:
            print(cell)
