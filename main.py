from p5 import *
from random import randint
from simulation.canvas import Canvas
from simulation.constants import *

canvas = None

def setup():
    size(WIN_WIDTH, WIN_HEIGHT)

    global canvas
    canvas = Canvas((CANVAS_XOFFSET, CANVAS_YOFFSET), WIN_HEIGHT, WIN_WIDTH)
    canvas.generate_cells(CELL_NUMBERS)
    canvas.cells[0].infected = True

def draw():
    background(0)

    canvas.draw_canvas()
    canvas.draw_cells()
    canvas.move_cells()
    canvas.spread()
    if canvas.has_finished():
        canvas.get_profiles()
        exit()

if __name__ == "__main__":
    run()
