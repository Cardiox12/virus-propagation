# **************************************************************************** #
#                                                                              #
#                                                         :::      ::::::::    #
#    virus.py                                           :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: tony <tony@student.42.fr>                  +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2020/03/26 23:00:31 by tony              #+#    #+#              #
#    Updated: 2020/03/29 06:27:25 by tony             ###   ########.fr        #
#                                                                              #
# **************************************************************************** #

class Virus():
    def __init__(self, name, incubation, infection_probs, recover_time):
        self.name               = name
        self.infection_probs    = infection_probs
        self.incubation         = incubation
        self.recover_time       = recover_time

    def __str__(self):
        return f"{self.name} :\nInfection probability : {self.infection_probs}\nIncubation time : {self.incubation}\nRecover time : {self.recover_time}\n\
                "
